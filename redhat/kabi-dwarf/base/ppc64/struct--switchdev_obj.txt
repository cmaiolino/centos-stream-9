Version: 1.0
File: include/net/switchdev.h:82
Symbol:
Byte size 32
struct switchdev_obj {
0x0 orig_dev * @"struct--net_device.txt"
0x8 id @"enum--switchdev_obj_id.txt"
0xc flags @"typedef--u32.txt"
0x10 complete_priv * "void"
0x18 complete * func (NULL) (
(NULL) * @"struct--net_device.txt"
(NULL) "int"
(NULL) * "void"
)
"void"
}
