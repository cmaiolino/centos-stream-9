Version: 1.0
File: include/linux/interrupt.h:542
Symbol:
Byte size 40
struct tasklet_struct {
0x0 next * @"struct--tasklet_struct.txt"
0x8 state "long unsigned int"
0x10 count @"typedef--atomic_t.txt"
0x18 func * func (NULL) (
(NULL) "long unsigned int"
)
"void"
0x20 data "long unsigned int"
}
