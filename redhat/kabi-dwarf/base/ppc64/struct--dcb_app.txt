Version: 1.0
File: include/uapi/linux/dcbnl.h:238
Symbol:
Byte size 4
struct dcb_app {
0x0 selector @"typedef--__u8.txt"
0x1 priority @"typedef--__u8.txt"
0x2 protocol @"typedef--__u16.txt"
}
