Version: 1.0
File: include/net/netns/core.h:7
Symbol:
Byte size 24
struct netns_core {
0x0 sysctl_hdr * @"struct--ctl_table_header.txt"
0x8 sysctl_somaxconn "int"
0x10 inuse * @"struct--prot_inuse.txt"
}
