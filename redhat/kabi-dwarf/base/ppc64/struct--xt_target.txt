Version: 1.0
File: include/linux/netfilter/x_tables.h:184
Symbol:
Byte size 152
struct xt_target {
0x0 list @"struct--list_head.txt"
0x10 name const [29]"char"
0x2d revision @"typedef--u_int8_t.txt"
0x30 target * func (NULL) (
(NULL) * @"struct--sk_buff.txt"
(NULL) * const @"struct--xt_action_param.txt"
)
"unsigned int"
0x38 checkentry * func (NULL) (
(NULL) * const @"struct--xt_tgchk_param.txt"
)
"int"
0x40 destroy * func (NULL) (
(NULL) * const @"struct--xt_tgdtor_param.txt"
)
"void"
0x48 compat_from_user * func (NULL) (
(NULL) * "void"
(NULL) * const "void"
)
"void"
0x50 compat_to_user * func (NULL) (
(NULL) * "void"
(NULL) * const "void"
)
"int"
0x58 me * @"struct--module.txt"
0x60 table * const "char"
0x68 targetsize "unsigned int"
0x6c compatsize "unsigned int"
0x70 hooks "unsigned int"
0x74 proto "short unsigned int"
0x76 family "short unsigned int"
0x78 rh_reserved1 "long unsigned int"
0x80 rh_reserved2 "long unsigned int"
0x88 rh_reserved3 "long unsigned int"
0x90 rh_reserved4 "long unsigned int"
}
