Version: 1.0
File: include/linux/pci.h:63
Symbol:
Byte size 104
struct pci_slot {
0x0 bus * @"struct--pci_bus.txt"
0x8 list @"struct--list_head.txt"
0x18 hotplug * @"struct--hotplug_slot.txt"
0x20 number "unsigned char"
0x28 kobj @"struct--kobject.txt"
}
