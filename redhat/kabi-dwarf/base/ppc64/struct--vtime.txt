Version: 1.0
File: include/linux/sched.h:816
Symbol:
Byte size 48
struct vtime {
0x0 seqlock @"typedef--seqlock_t.txt"
0x8 starttime "long long unsigned int"
0x10 state @"enum--vtime_state.txt"
0x18 utime @"typedef--u64.txt"
0x20 stime @"typedef--u64.txt"
0x28 gtime @"typedef--u64.txt"
}
