Version: 1.0
File: kernel/sched/sched.h:578
Symbol:
Byte size 36480
struct root_domain {
0x0 refcount @"typedef--atomic_t.txt"
0x4 rto_count @"typedef--atomic_t.txt"
0x8 rcu @"struct--callback_head.txt"
0x18 span @"typedef--cpumask_var_t.txt"
0x118 online @"typedef--cpumask_var_t.txt"
0x218 rto_mask @"typedef--cpumask_var_t.txt"
0x318 cpupri @"struct--cpupri.txt"
0x8c48 overload @"typedef--bool.txt"
0x8c50 dlo_mask @"typedef--cpumask_var_t.txt"
0x8d50 dlo_count @"typedef--atomic_t.txt"
0x8d58 dl_bw @"struct--dl_bw.txt"
0x8d70 cpudl @"struct--cpudl.txt"
}
