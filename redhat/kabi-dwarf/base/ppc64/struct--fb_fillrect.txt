Version: 1.0
File: include/uapi/linux/fb.h:349
Symbol:
Byte size 24
struct fb_fillrect {
0x0 dx @"typedef--__u32.txt"
0x4 dy @"typedef--__u32.txt"
0x8 width @"typedef--__u32.txt"
0xc height @"typedef--__u32.txt"
0x10 color @"typedef--__u32.txt"
0x14 rop @"typedef--__u32.txt"
}
