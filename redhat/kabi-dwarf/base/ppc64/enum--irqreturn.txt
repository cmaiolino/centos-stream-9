Version: 1.0
File: include/linux/irqreturn.h:10
Symbol:
Byte size 4
enum irqreturn {
IRQ_NONE = 0x0
IRQ_HANDLED = 0x1
IRQ_WAKE_THREAD = 0x2
}
