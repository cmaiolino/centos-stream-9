Version: 1.0
File: include/uapi/scsi/scsi_bsg_fc.h:143
Symbol:
Byte size 8
struct fc_bsg_ctels_reply {
0x0 status @"typedef--uint32_t.txt"
0x4 rjt_data struct (NULL) {
0x0 action @"typedef--uint8_t.txt"
0x1 reason_code @"typedef--uint8_t.txt"
0x2 reason_explanation @"typedef--uint8_t.txt"
0x3 vendor_unique @"typedef--uint8_t.txt"
}
}
