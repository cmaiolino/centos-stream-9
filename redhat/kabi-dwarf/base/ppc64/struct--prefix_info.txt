Version: 1.0
File: include/net/addrconf.h:27
Symbol:
Byte size 32
struct prefix_info {
0x0 type @"typedef--__u8.txt"
0x1 length @"typedef--__u8.txt"
0x2 prefix_len @"typedef--__u8.txt"
0x3:0-1 onlink @"typedef--__u8.txt"
0x3:1-2 autoconf @"typedef--__u8.txt"
0x3:2-8 reserved @"typedef--__u8.txt"
0x4 valid @"typedef--__be32.txt"
0x8 prefered @"typedef--__be32.txt"
0xc reserved2 @"typedef--__be32.txt"
0x10 prefix @"struct--in6_addr.txt"
}
