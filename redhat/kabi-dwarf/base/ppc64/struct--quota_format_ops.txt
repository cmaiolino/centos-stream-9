Version: 1.0
File: include/linux/quota.h:305
Symbol:
Byte size 56
struct quota_format_ops {
0x0 check_quota_file * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x8 read_file_info * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x10 write_file_info * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x18 free_file_info * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x20 read_dqblk * func (NULL) (
(NULL) * @"struct--dquot.txt"
)
"int"
0x28 commit_dqblk * func (NULL) (
(NULL) * @"struct--dquot.txt"
)
"int"
0x30 release_dqblk * func (NULL) (
(NULL) * @"struct--dquot.txt"
)
"int"
}
