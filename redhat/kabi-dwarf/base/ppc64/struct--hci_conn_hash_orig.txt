Version: 1.0
File: include/net/bluetooth/hci_core.h:120
Symbol:
Byte size 32
struct hci_conn_hash_orig {
0x0 list @"struct--list_head.txt"
0x10 acl_num "unsigned int"
0x14 amp_num "unsigned int"
0x18 sco_num "unsigned int"
0x1c le_num "unsigned int"
}
