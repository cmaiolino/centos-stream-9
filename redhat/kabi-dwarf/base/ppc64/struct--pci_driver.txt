Version: 1.0
File: include/linux/pci.h:726
Symbol:
Byte size 264
struct pci_driver {
0x0 node @"struct--list_head.txt"
0x10 name * const "char"
0x18 id_table * const @"struct--pci_device_id.txt"
0x20 probe * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
(NULL) * const @"struct--pci_device_id.txt"
)
"int"
0x28 remove * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
"void"
0x30 suspend * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
(NULL) @"typedef--pm_message_t.txt"
)
"int"
0x38 suspend_late * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
(NULL) @"typedef--pm_message_t.txt"
)
"int"
0x40 resume_early * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
"int"
0x48 resume * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
"int"
0x50 shutdown * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
"void"
0x58 sriov_configure * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
(NULL) "int"
)
"int"
0x60 err_handler * const @"struct--pci_error_handlers.txt"
0x68 driver @"struct--device_driver.txt"
0xe8 dynids @"struct--pci_dynids.txt"
0x100 pci_driver_rh * @"struct--pci_driver_rh.txt"
}
