Version: 1.0
File: include/linux/mmzone.h:266
Symbol:
Byte size 4
enum zone_type {
ZONE_DMA = 0x0
ZONE_NORMAL = 0x1
ZONE_MOVABLE = 0x2
__MAX_NR_ZONES = 0x3
REAL_MAX_ZONES = 0x3
}
