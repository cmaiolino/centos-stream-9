Version: 1.0
File: include/linux/ipv6.h:11
Symbol:
Byte size 168
struct ipv6_devconf {
0x0 forwarding @"typedef--__s32.txt"
0x4 hop_limit @"typedef--__s32.txt"
0x8 mtu6 @"typedef--__s32.txt"
0xc accept_ra @"typedef--__s32.txt"
0x10 accept_redirects @"typedef--__s32.txt"
0x14 autoconf @"typedef--__s32.txt"
0x18 dad_transmits @"typedef--__s32.txt"
0x1c rtr_solicits @"typedef--__s32.txt"
0x20 rtr_solicit_interval @"typedef--__s32.txt"
0x24 rtr_solicit_delay @"typedef--__s32.txt"
0x28 force_mld_version @"typedef--__s32.txt"
0x2c mldv1_unsolicited_report_interval @"typedef--__s32.txt"
0x30 mldv2_unsolicited_report_interval @"typedef--__s32.txt"
0x34 use_tempaddr @"typedef--__s32.txt"
0x38 temp_valid_lft @"typedef--__s32.txt"
0x3c temp_prefered_lft @"typedef--__s32.txt"
0x40 regen_max_retry @"typedef--__s32.txt"
0x44 max_desync_factor @"typedef--__s32.txt"
0x48 max_addresses @"typedef--__s32.txt"
0x4c accept_ra_defrtr @"typedef--__s32.txt"
0x50 accept_ra_pinfo @"typedef--__s32.txt"
0x54 accept_ra_rtr_pref @"typedef--__s32.txt"
0x58 rtr_probe_interval @"typedef--__s32.txt"
0x5c accept_ra_rt_info_max_plen @"typedef--__s32.txt"
0x60 proxy_ndp @"typedef--__s32.txt"
0x64 accept_source_route @"typedef--__s32.txt"
0x68 optimistic_dad @"typedef--__s32.txt"
0x6c mc_forwarding @"typedef--__s32.txt"
0x70 disable_ipv6 @"typedef--__s32.txt"
0x74 accept_dad @"typedef--__s32.txt"
0x78 force_tllao @"typedef--__s32.txt"
0x7c ndisc_notify @"typedef--__s32.txt"
0x80 stable_secret @"struct--ipv6_stable_secret.txt"
0x94 use_optimistic @"typedef--__s32.txt"
0x98 keep_addr_on_down @"typedef--__s32.txt"
0x9c enhanced_dad @"typedef--__u32.txt"
0xa0 sysctl * "void"
}
