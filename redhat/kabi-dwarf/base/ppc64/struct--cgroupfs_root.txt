Version: 1.0
File: include/linux/cgroup.h:331
Symbol:
Byte size 4712
struct cgroupfs_root {
0x0 sb * @"struct--super_block.txt"
0x8 subsys_mask "long unsigned int"
0x10 hierarchy_id "int"
0x18 actual_subsys_mask "long unsigned int"
0x20 subsys_list @"struct--list_head.txt"
0x30 top_cgroup @"struct--cgroup.txt"
0x1c8 number_of_cgroups "int"
0x1d0 root_list @"struct--list_head.txt"
0x1e0 allcg_list @"struct--list_head.txt"
0x1f0 flags "long unsigned int"
0x1f8 cgroup_ida @"struct--ida.txt"
0x228 release_agent_path [4096]"char"
0x1228 name [64]"char"
}
