Version: 1.0
File: include/linux/bpf.h:45
Symbol:
Byte size 256
struct bpf_map {
0x0 ops * const @"struct--bpf_map_ops.txt"
0x8 inner_map_meta * @"struct--bpf_map.txt"
0x10 security * "void"
0x18 map_type @"enum--bpf_map_type.txt"
0x1c key_size @"typedef--u32.txt"
0x20 value_size @"typedef--u32.txt"
0x24 max_entries @"typedef--u32.txt"
0x28 map_flags @"typedef--u32.txt"
0x2c pages @"typedef--u32.txt"
0x30 id @"typedef--u32.txt"
0x34 numa_node "int"
0x38 unpriv_array @"typedef--bool.txt"
0x80 user * @"struct--user_struct.txt"
0x88 refcnt @"typedef--atomic_t.txt"
0x8c usercnt @"typedef--atomic_t.txt"
0x90 work @"struct--work_struct.txt"
0xb0 name [16]"char"
}
