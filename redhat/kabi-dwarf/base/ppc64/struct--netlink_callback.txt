Version: 1.0
File: include/linux/netlink.h:136
Symbol:
Byte size 112
struct netlink_callback {
0x0 skb * @"struct--sk_buff.txt"
0x8 nlh * const @"struct--nlmsghdr.txt"
0x10 dump * func (NULL) (
(NULL) * @"struct--sk_buff.txt"
(NULL) * @"struct--netlink_callback.txt"
)
"int"
0x18 done * func (NULL) (
(NULL) * @"struct--netlink_callback.txt"
)
"int"
0x20 data * "void"
0x28 module * @"struct--module.txt"
0x30 family @"typedef--u16.txt"
0x32 min_dump_alloc @"typedef--u16.txt"
0x34 prev_seq "unsigned int"
0x38 seq "unsigned int"
0x40 args [6]"long int"
}
