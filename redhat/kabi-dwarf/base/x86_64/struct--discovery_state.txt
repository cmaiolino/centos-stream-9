Version: 1.0
File: include/net/bluetooth/hci_core.h:83
Symbol:
Byte size 136
struct discovery_state {
0x0 type "int"
0x4 state enum (NULL) {
DISCOVERY_STOPPED = 0x0
DISCOVERY_STARTING = 0x1
DISCOVERY_FINDING = 0x2
DISCOVERY_RESOLVING = 0x3
DISCOVERY_STOPPING = 0x4
}
0x8 all @"struct--list_head.txt"
0x18 unknown @"struct--list_head.txt"
0x28 resolve @"struct--list_head.txt"
0x38 timestamp @"typedef--__u32.txt"
0x3c last_adv_addr @"typedef--bdaddr_t.txt"
0x42 last_adv_addr_type @"typedef--u8.txt"
0x43 last_adv_rssi @"typedef--s8.txt"
0x44 last_adv_flags @"typedef--u32.txt"
0x48 last_adv_data [31]@"typedef--u8.txt"
0x67 last_adv_data_len @"typedef--u8.txt"
0x68 report_invalid_rssi @"typedef--bool.txt"
0x69 result_filtering @"typedef--bool.txt"
0x6a limited @"typedef--bool.txt"
0x6b rssi @"typedef--s8.txt"
0x6c uuid_count @"typedef--u16.txt"
0x70 uuids * [16]@"typedef--u8.txt"
0x78 scan_start "long unsigned int"
0x80 scan_duration "long unsigned int"
}
