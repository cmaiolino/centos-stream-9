Version: 1.0
File: include/linux/sched.h:521
Symbol:
Byte size 24
struct task_cputime {
0x0 utime @"typedef--cputime_t.txt"
0x8 stime @"typedef--cputime_t.txt"
0x10 sum_exec_runtime "long long unsigned int"
}
