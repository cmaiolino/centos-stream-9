Version: 1.0
File: include/linux/relay.h:56
Symbol:
Byte size 41320
struct rchan {
0x0 version @"typedef--u32.txt"
0x8 subbuf_size @"typedef--size_t.txt"
0x10 n_subbufs @"typedef--size_t.txt"
0x18 alloc_size @"typedef--size_t.txt"
0x20 cb * @"struct--rchan_callbacks.txt"
0x28 kref @"struct--kref.txt"
0x30 private_data * "void"
0x38 last_toobig @"typedef--size_t.txt"
0x40 buf [5120]* @"struct--rchan_buf.txt"
0xa040 is_global "int"
0xa048 list @"struct--list_head.txt"
0xa058 parent * @"struct--dentry.txt"
0xa060 has_base_filename "int"
0xa064 base_filename [255]"char"
}
