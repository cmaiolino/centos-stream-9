Version: 1.0
File: include/net/netns/conntrack.h:89
Symbol:
Byte size 224
struct nf_ip_net {
0x0 generic @"struct--nf_generic_net.txt"
0x20 tcp @"struct--nf_tcp_net.txt"
0x80 udp @"struct--nf_udp_net.txt"
0xa0 icmp @"struct--nf_icmp_net.txt"
0xc0 icmpv6 @"struct--nf_icmp_net.txt"
}
