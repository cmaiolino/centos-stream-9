Version: 1.0
File: include/linux/pm_qos.h:62
Symbol:
Byte size 56
struct dev_pm_qos_request {
0x0 type @"enum--dev_pm_qos_req_type.txt"
0x8 data union (NULL) {
pnode @"struct--plist_node.txt"
flr @"struct--pm_qos_flags_request.txt"
}
0x30 dev * @"struct--device.txt"
}
