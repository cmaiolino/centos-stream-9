Version: 1.0
File: include/linux/mempolicy.h:44
Symbol:
Byte size 264
struct mempolicy {
0x0 refcnt @"typedef--atomic_t.txt"
0x4 mode "short unsigned int"
0x6 flags "short unsigned int"
0x8 v union (NULL) {
preferred_node "short int"
nodes @"typedef--nodemask_t.txt"
}
0x88 w union (NULL) {
cpuset_mems_allowed @"typedef--nodemask_t.txt"
user_nodemask @"typedef--nodemask_t.txt"
}
}
