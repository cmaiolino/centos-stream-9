Version: 1.0
File: include/uapi/linux/ethtool.h:99
Symbol:
Byte size 44
struct ethtool_cmd {
0x0 cmd @"typedef--__u32.txt"
0x4 supported @"typedef--__u32.txt"
0x8 advertising @"typedef--__u32.txt"
0xc speed @"typedef--__u16.txt"
0xe duplex @"typedef--__u8.txt"
0xf port @"typedef--__u8.txt"
0x10 phy_address @"typedef--__u8.txt"
0x11 transceiver @"typedef--__u8.txt"
0x12 autoneg @"typedef--__u8.txt"
0x13 mdio_support @"typedef--__u8.txt"
0x14 maxtxpkt @"typedef--__u32.txt"
0x18 maxrxpkt @"typedef--__u32.txt"
0x1c speed_hi @"typedef--__u16.txt"
0x1e eth_tp_mdix @"typedef--__u8.txt"
0x1f eth_tp_mdix_ctrl @"typedef--__u8.txt"
0x20 lp_advertising @"typedef--__u32.txt"
0x24 reserved [2]@"typedef--__u32.txt"
}
