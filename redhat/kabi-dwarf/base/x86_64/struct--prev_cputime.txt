Version: 1.0
File: include/linux/sched.h:489
Symbol:
Byte size 24
struct prev_cputime {
0x0 utime @"typedef--cputime_t.txt"
0x8 stime @"typedef--cputime_t.txt"
0x10 lock @"typedef--raw_spinlock_t.txt"
}
