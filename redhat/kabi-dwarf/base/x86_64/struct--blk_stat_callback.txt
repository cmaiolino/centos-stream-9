Version: 1.0
File: block/blk-stat.h:26
Symbol:
Byte size 160
struct blk_stat_callback {
0x0 list @"struct--list_head.txt"
0x10 timer @"struct--timer_list.txt"
0x60 cpu_stat * @"struct--blk_rq_stat.txt"
0x68 bucket_fn * func (NULL) (
(NULL) * const @"struct--request.txt"
)
"int"
0x70 buckets "unsigned int"
0x78 stat * @"struct--blk_rq_stat.txt"
0x80 timer_fn * func (NULL) (
(NULL) * @"struct--blk_stat_callback.txt"
)
"void"
0x88 data * "void"
0x90 rcu @"struct--callback_head.txt"
}
