Version: 1.0
File: include/uapi/linux/dcbnl.h:189
Symbol:
Byte size 20
struct cee_pg {
0x0 willing @"typedef--__u8.txt"
0x1 error @"typedef--__u8.txt"
0x2 pg_en @"typedef--__u8.txt"
0x3 tcs_supported @"typedef--__u8.txt"
0x4 pg_bw [8]@"typedef--__u8.txt"
0xc prio_pg [8]@"typedef--__u8.txt"
}
