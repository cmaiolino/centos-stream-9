Version: 1.0
File: include/linux/ipmi_smi.h:150
Symbol:
Byte size 24
struct ipmi_device_id {
0x0 device_id "unsigned char"
0x1 device_revision "unsigned char"
0x2 firmware_revision_1 "unsigned char"
0x3 firmware_revision_2 "unsigned char"
0x4 ipmi_version "unsigned char"
0x5 additional_device_support "unsigned char"
0x8 manufacturer_id "unsigned int"
0xc product_id "unsigned int"
0x10 aux_firmware_revision [4]"unsigned char"
0x14:0-1 aux_firmware_revision_set "unsigned int"
}
