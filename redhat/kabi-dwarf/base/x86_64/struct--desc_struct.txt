Version: 1.0
File: arch/x86/include/asm/desc_defs.h:22
Symbol:
Byte size 8
struct desc_struct {
0x0 (NULL) union (NULL) {
(NULL) struct (NULL) {
0x0 a "unsigned int"
0x4 b "unsigned int"
}
(NULL) struct (NULL) {
0x0 limit0 @"typedef--u16.txt"
0x2 base0 @"typedef--u16.txt"
0x4:0-8 base1 "unsigned int"
0x5:0-4 type "unsigned int"
0x5:4-5 s "unsigned int"
0x5:5-7 dpl "unsigned int"
0x5:7-8 p "unsigned int"
0x6:0-4 limit "unsigned int"
0x6:4-5 avl "unsigned int"
0x6:5-6 l "unsigned int"
0x6:6-7 d "unsigned int"
0x6:7-8 g "unsigned int"
0x7:0-8 base2 "unsigned int"
}
}
}
