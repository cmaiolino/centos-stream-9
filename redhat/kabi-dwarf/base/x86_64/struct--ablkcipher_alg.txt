Version: 1.0
File: include/linux/crypto.h:225
Symbol:
Byte size 64
struct ablkcipher_alg {
0x0 setkey * func (NULL) (
(NULL) * @"struct--crypto_ablkcipher.txt"
(NULL) * const @"typedef--u8.txt"
(NULL) "unsigned int"
)
"int"
0x8 encrypt * func (NULL) (
(NULL) * @"struct--ablkcipher_request.txt"
)
"int"
0x10 decrypt * func (NULL) (
(NULL) * @"struct--ablkcipher_request.txt"
)
"int"
0x18 givencrypt * func (NULL) (
(NULL) * @"<declarations>/struct--skcipher_givcrypt_request.txt"
)
"int"
0x20 givdecrypt * func (NULL) (
(NULL) * @"<declarations>/struct--skcipher_givcrypt_request.txt"
)
"int"
0x28 geniv * const "char"
0x30 min_keysize "unsigned int"
0x34 max_keysize "unsigned int"
0x38 ivsize "unsigned int"
}
