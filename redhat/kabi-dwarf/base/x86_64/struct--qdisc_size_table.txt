Version: 1.0
File: include/net/sch_generic.h:52
Symbol:
Byte size 64
struct qdisc_size_table {
0x0 rcu @"struct--callback_head.txt"
0x10 list @"struct--list_head.txt"
0x20 szopts @"struct--tc_sizespec.txt"
0x38 refcnt "int"
0x3c data [0]@"typedef--u16.txt"
}
