Version: 1.0
File: include/linux/mmu_notifier.h:29
Symbol:
Byte size 56
struct mmu_notifier_ops {
0x0 release * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
)
"void"
0x8 clear_flush_young * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
)
"int"
0x10 test_young * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
)
"int"
0x18 change_pte * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
(NULL) @"typedef--pte_t.txt"
)
"void"
0x20 invalidate_page * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
)
"void"
0x28 invalidate_range_start * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
(NULL) "long unsigned int"
)
"void"
0x30 invalidate_range_end * func (NULL) (
(NULL) * @"struct--mmu_notifier.txt"
(NULL) * @"struct--mm_struct.txt"
(NULL) "long unsigned int"
(NULL) "long unsigned int"
)
"void"
}
