Version: 1.0
File: include/linux/device.h:47
Symbol:
Byte size 32
struct bus_attribute {
0x0 attr @"struct--attribute.txt"
0x10 show * func (NULL) (
(NULL) * @"struct--bus_type.txt"
(NULL) * "char"
)
@"typedef--ssize_t.txt"
0x18 store * func (NULL) (
(NULL) * @"struct--bus_type.txt"
(NULL) * const "char"
(NULL) @"typedef--size_t.txt"
)
@"typedef--ssize_t.txt"
}
