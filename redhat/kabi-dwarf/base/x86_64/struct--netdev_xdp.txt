Version: 1.0
File: include/linux/netdevice.h:1044
Symbol:
Byte size 24
struct netdev_xdp {
0x0 command @"enum--xdp_netdev_command.txt"
0x8 (NULL) union (NULL) {
prog * @"struct--bpf_prog.txt"
(NULL) struct (NULL) {
0x0 prog_id @"typedef--u32.txt"
}
xsk struct (NULL) {
0x0 umem * @"<declarations>/struct--xdp_umem.txt"
0x8 queue_id @"typedef--u16.txt"
}
}
}
