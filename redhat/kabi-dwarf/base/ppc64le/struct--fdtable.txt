Version: 1.0
File: include/linux/fdtable.h:25
Symbol:
Byte size 48
struct fdtable {
0x0 max_fds "unsigned int"
0x8 fd * * @"struct--file.txt"
0x10 close_on_exec * "long unsigned int"
0x18 open_fds * "long unsigned int"
0x20 (NULL) union (NULL) {
rcu @"struct--callback_head.txt"
full_fds_bits * "long unsigned int"
}
}
