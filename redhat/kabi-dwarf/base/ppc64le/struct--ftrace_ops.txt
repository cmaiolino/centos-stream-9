Version: 1.0
File: include/linux/ftrace.h:120
Symbol:
Byte size 88
struct ftrace_ops {
0x0 func @"typedef--ftrace_func_t.txt"
0x8 next * @"struct--ftrace_ops.txt"
0x10 flags "long unsigned int"
0x18 disabled * "int"
0x20 notrace_hash * @"<declarations>/struct--ftrace_hash.txt"
0x28 filter_hash * @"<declarations>/struct--ftrace_hash.txt"
0x30 regex_lock @"struct--mutex.txt"
}
