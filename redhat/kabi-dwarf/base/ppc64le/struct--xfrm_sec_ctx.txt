Version: 1.0
File: include/uapi/linux/xfrm.h:32
Symbol:
Byte size 8
struct xfrm_sec_ctx {
0x0 ctx_doi @"typedef--__u8.txt"
0x1 ctx_alg @"typedef--__u8.txt"
0x2 ctx_len @"typedef--__u16.txt"
0x4 ctx_sid @"typedef--__u32.txt"
0x8 ctx_str [0]"char"
}
