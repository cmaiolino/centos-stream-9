Version: 1.0
File: include/net/net_namespace.h:340
Symbol:
Byte size 56
struct pernet_operations {
0x0 list @"struct--list_head.txt"
0x10 init * func (NULL) (
(NULL) * @"struct--net.txt"
)
"int"
0x18 exit * func (NULL) (
(NULL) * @"struct--net.txt"
)
"void"
0x20 exit_batch * func (NULL) (
(NULL) * @"struct--list_head.txt"
)
"void"
0x28 id * "int"
0x30 size @"typedef--size_t.txt"
}
