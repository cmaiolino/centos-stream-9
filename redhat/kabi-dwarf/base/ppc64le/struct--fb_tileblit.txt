Version: 1.0
File: include/linux/fb.h:346
Symbol:
Byte size 40
struct fb_tileblit {
0x0 sx @"typedef--__u32.txt"
0x4 sy @"typedef--__u32.txt"
0x8 width @"typedef--__u32.txt"
0xc height @"typedef--__u32.txt"
0x10 fg @"typedef--__u32.txt"
0x14 bg @"typedef--__u32.txt"
0x18 length @"typedef--__u32.txt"
0x20 indices * @"typedef--__u32.txt"
}
