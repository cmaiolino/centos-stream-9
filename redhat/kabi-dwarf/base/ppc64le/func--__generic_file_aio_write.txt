Version: 1.0
File: mm/filemap.c:3076
Symbol:
func __generic_file_aio_write (
iocb * @"struct--kiocb.txt"
iov * const @"struct--iovec.txt"
nr_segs "long unsigned int"
ppos * @"typedef--loff_t.txt"
)
@"typedef--ssize_t.txt"
