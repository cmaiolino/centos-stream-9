Version: 1.0
File: include/linux/perf_event.h:817
Symbol:
Byte size 640
struct perf_sample_data {
0x0 type @"typedef--u64.txt"
0x8 ip @"typedef--u64.txt"
0x10 tid_entry struct (NULL) {
0x0 pid @"typedef--u32.txt"
0x4 tid @"typedef--u32.txt"
}
0x18 time @"typedef--u64.txt"
0x20 addr @"typedef--u64.txt"
0x28 id @"typedef--u64.txt"
0x30 stream_id @"typedef--u64.txt"
0x38 cpu_entry struct (NULL) {
0x0 cpu @"typedef--u32.txt"
0x4 reserved @"typedef--u32.txt"
}
0x40 period @"typedef--u64.txt"
0x48 data_src @"union--perf_mem_data_src.txt"
0x50 callchain * @"struct--perf_callchain_entry.txt"
0x58 raw * @"struct--perf_raw_record.txt"
0x60 br_stack * @"struct--perf_branch_stack.txt"
0x68 (NULL) union (NULL) {
regs_user @"struct--perf_regs.txt"
__UNIQUE_ID_rh_kabi_hide37 struct (NULL) {
0x0 regs_user @"struct--perf_regs_user.txt"
}
(NULL) union (NULL) {
}
}
0x78 stack_user_size @"typedef--u64.txt"
0x80 weight @"typedef--u64.txt"
0x88 txn @"typedef--u64.txt"
0x90 regs_intr @"struct--perf_regs.txt"
0xa0 regs_user_copy @"struct--pt_regs.txt"
0x200 phys_addr @"typedef--u64.txt"
}
