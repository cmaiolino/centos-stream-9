Version: 1.0
File: include/linux/perf_event.h:694
Symbol:
Byte size 464
struct perf_cpu_context {
0x0 ctx @"struct--perf_event_context.txt"
0x130 task_ctx * @"struct--perf_event_context.txt"
0x138 active_oncpu "int"
0x13c exclusive "int"
0x140 hrtimer @"struct--hrtimer.txt"
0x1a0 hrtimer_interval @"typedef--ktime_t.txt"
0x1a8 rh_reserved_rotation_list @"struct--list_head.txt"
0x1b8 unique_pmu * @"struct--pmu.txt"
0x1c0 cgrp * @"struct--perf_cgroup.txt"
0x1c8 hrtimer_lock @"typedef--raw_spinlock_t.txt"
0x1cc hrtimer_active "unsigned int"
}
