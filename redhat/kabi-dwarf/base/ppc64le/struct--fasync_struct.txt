Version: 1.0
File: include/linux/fs.h:1304
Symbol:
Byte size 48
struct fasync_struct {
0x0 fa_lock @"typedef--spinlock_t.txt"
0x4 magic "int"
0x8 fa_fd "int"
0x10 fa_next * @"struct--fasync_struct.txt"
0x18 fa_file * @"struct--file.txt"
0x20 fa_rcu @"struct--callback_head.txt"
}
