Version: 1.0
File: include/linux/quota.h:332
Symbol:
Byte size 104
struct quotactl_ops {
0x0 quota_on * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
(NULL) "int"
(NULL) * @"struct--path.txt"
)
"int"
0x8 quota_on_meta * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
(NULL) "int"
)
"int"
0x10 quota_off * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x18 quota_sync * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
)
"int"
0x20 get_info * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
(NULL) * @"struct--if_dqinfo.txt"
)
"int"
0x28 set_info * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "int"
(NULL) * @"struct--if_dqinfo.txt"
)
"int"
0x30 get_dqblk * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) @"struct--kqid.txt"
(NULL) * @"struct--fs_disk_quota.txt"
)
"int"
0x38 set_dqblk * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) @"struct--kqid.txt"
(NULL) * @"struct--fs_disk_quota.txt"
)
"int"
0x40 get_xstate * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) * @"struct--fs_quota_stat.txt"
)
"int"
0x48 set_xstate * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "unsigned int"
(NULL) "int"
)
"int"
0x50 get_xstatev * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) * @"struct--fs_quota_statv.txt"
)
"int"
0x58 rm_xquota * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) "unsigned int"
)
"int"
0x60 get_nextdqblk * func (NULL) (
(NULL) * @"struct--super_block.txt"
(NULL) * @"struct--kqid.txt"
(NULL) * @"struct--fs_disk_quota.txt"
)
"int"
}
