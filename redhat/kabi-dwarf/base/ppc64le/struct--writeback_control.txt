Version: 1.0
File: include/linux/writeback.h:63
Symbol:
Byte size 56
struct writeback_control {
0x0 nr_to_write "long int"
0x8 pages_skipped "long int"
0x10 range_start @"typedef--loff_t.txt"
0x18 range_end @"typedef--loff_t.txt"
0x20 sync_mode @"enum--writeback_sync_modes.txt"
0x24:0-1 for_kupdate "unsigned int"
0x24:1-2 for_background "unsigned int"
0x24:2-3 tagged_writepages "unsigned int"
0x24:3-4 for_reclaim "unsigned int"
0x24:4-5 range_cyclic "unsigned int"
0x24:5-6 for_sync "unsigned int"
0x28 rh_reserved1 "long unsigned int"
0x30 rh_reserved2 "long unsigned int"
}
