Version: 1.0
File: include/linux/phy.h:351
Symbol:
Byte size 1080
struct phy_device {
0x0 drv * @"struct--phy_driver.txt"
0x8 mdio_bus * @"struct--mii_bus.txt"
0x10 mdio_dev @"struct--device.txt"
0x2c0 phy_id @"typedef--u32.txt"
0x2c4 c45_ids @"struct--phy_c45_device_ids.txt"
0x2e8 is_c45 @"typedef--bool.txt"
0x2e9 is_internal @"typedef--bool.txt"
0x2ea suspended @"typedef--bool.txt"
0x2ec state @"enum--phy_state.txt"
0x2f0 dev_flags @"typedef--u32.txt"
0x2f4 interface @"typedef--phy_interface_t.txt"
0x2f8 mdio_addr "int"
0x2fc speed "int"
0x300 duplex "int"
0x304 pause "int"
0x308 asym_pause "int"
0x30c link "int"
0x310 interrupts @"typedef--u32.txt"
0x314 supported @"typedef--u32.txt"
0x318 advertising @"typedef--u32.txt"
0x31c autoneg "int"
0x320 link_timeout "int"
0x324 irq "int"
0x328 priv * "void"
0x330 phy_queue @"struct--work_struct.txt"
0x350 state_queue @"struct--delayed_work.txt"
0x3d0 irq_disable @"typedef--atomic_t.txt"
0x3d8 lock @"struct--mutex.txt"
0x400 attached_dev * @"struct--net_device.txt"
0x408 adjust_link * func (NULL) (
(NULL) * @"struct--net_device.txt"
)
"void"
0x410 rh_reserved_adjust_state * func (NULL) (
(NULL) * @"struct--net_device.txt"
)
"void"
0x418 lp_advertising @"typedef--u32.txt"
0x41c mdix @"typedef--u8.txt"
0x420 mdio_flags "int"
0x428 mdio_pm_ops * const @"struct--dev_pm_ops.txt"
0x430 eee_broken_modes @"typedef--u32.txt"
}
