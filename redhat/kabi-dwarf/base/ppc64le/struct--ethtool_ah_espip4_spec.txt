Version: 1.0
File: include/uapi/linux/ethtool.h:730
Symbol:
Byte size 16
struct ethtool_ah_espip4_spec {
0x0 ip4src @"typedef--__be32.txt"
0x4 ip4dst @"typedef--__be32.txt"
0x8 spi @"typedef--__be32.txt"
0xc tos @"typedef--__u8.txt"
}
