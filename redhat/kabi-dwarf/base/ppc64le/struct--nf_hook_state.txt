Version: 1.0
File: include/linux/netfilter.h:49
Symbol:
Byte size 64
struct nf_hook_state {
0x0 size @"typedef--size_t.txt"
0x8 hook "unsigned int"
0xc thresh "int"
0x10 pf @"typedef--u_int8_t.txt"
0x18 in * @"struct--net_device.txt"
0x20 out * @"struct--net_device.txt"
0x28 sk * @"struct--sock.txt"
0x30 okfn * func (NULL) (
(NULL) * @"struct--sock.txt"
(NULL) * @"struct--sk_buff.txt"
)
"int"
0x38 net * @"struct--net.txt"
}
