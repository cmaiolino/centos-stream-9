Version: 1.0
File: include/net/switchdev.h:57
Symbol:
Byte size 72
struct switchdev_attr {
0x0 orig_dev * @"struct--net_device.txt"
0x8 id @"enum--switchdev_attr_id.txt"
0xc flags @"typedef--u32.txt"
0x10 complete_priv * "void"
0x18 complete * func (NULL) (
(NULL) * @"struct--net_device.txt"
(NULL) "int"
(NULL) * "void"
)
"void"
0x20 u union (NULL) {
ppid @"struct--netdev_phys_port_id.txt"
stp_state @"typedef--u8.txt"
brport_flags "long unsigned int"
brport_flags_support "long unsigned int"
mrouter @"typedef--bool.txt"
ageing_time @"typedef--clock_t.txt"
vlan_filtering @"typedef--bool.txt"
mc_disabled @"typedef--bool.txt"
}
}
