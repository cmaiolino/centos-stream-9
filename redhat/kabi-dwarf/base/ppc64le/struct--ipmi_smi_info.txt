Version: 1.0
File: include/linux/ipmi.h:282
Symbol:
Byte size 24
struct ipmi_smi_info {
0x0 addr_src @"enum--ipmi_addr_src.txt"
0x8 dev * @"struct--device.txt"
0x10 addr_info @"union--ipmi_smi_info_union.txt"
}
