Version: 1.0
File: include/scsi/fcoe_sysfs.h:52
Symbol:
Byte size 4
enum ctlr_enabled_state {
FCOE_CTLR_ENABLED = 0x0
FCOE_CTLR_DISABLED = 0x1
FCOE_CTLR_UNUSED = 0x2
}
