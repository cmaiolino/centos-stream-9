Version: 1.0
File: include/linux/klist.h:20
Symbol:
Byte size 40
struct klist {
0x0 k_lock @"typedef--spinlock_t.txt"
0x8 k_list @"struct--list_head.txt"
0x18 get * func (NULL) (
(NULL) * @"struct--klist_node.txt"
)
"void"
0x20 put * func (NULL) (
(NULL) * @"struct--klist_node.txt"
)
"void"
}
