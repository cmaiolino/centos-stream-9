Version: 1.0
File: include/linux/pci.h:417
Symbol:
Byte size 48
struct pci_dev_rh {
0x0 dma_alias_mask * "long unsigned int"
0x8 driver_override * "char"
0x10 pci_data * @"struct--pci_dn.txt"
0x18:0-1 ats_enabled "unsigned int"
0x1a ats_cap @"typedef--u16.txt"
0x1c ats_stu @"typedef--u8.txt"
0x20 ats_ref_cnt @"typedef--atomic_t.txt"
0x28 priv_flags "long unsigned int"
}
