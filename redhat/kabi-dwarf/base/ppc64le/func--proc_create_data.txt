Version: 1.0
File: fs/proc/generic.c:537
Symbol:
func proc_create_data (
name * const "char"
mode @"typedef--umode_t.txt"
parent * @"struct--proc_dir_entry.txt"
proc_fops * const @"struct--file_operations.txt"
data * "void"
)
* @"struct--proc_dir_entry.txt"
