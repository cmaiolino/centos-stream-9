Version: 1.0
File: include/linux/mm.h:316
Symbol:
Byte size 88
struct vm_fault {
0x0 flags "unsigned int"
0x8 pgoff "long unsigned int"
0x10 virtual_address * "void"
0x18 page * @"struct--page.txt"
0x20 cow_page * @"struct--page.txt"
0x28 orig_pte @"typedef--pte_t.txt"
0x30 pmd * @"typedef--pmd_t.txt"
0x38 vma * @"struct--vm_area_struct.txt"
0x40 gfp_mask @"typedef--gfp_t.txt"
0x48 pte * @"typedef--pte_t.txt"
0x50 pud * @"typedef--pud_t.txt"
}
