Version: 1.0
File: include/uapi/linux/dcbnl.h:206
Symbol:
Byte size 4
struct cee_pfc {
0x0 willing @"typedef--__u8.txt"
0x1 error @"typedef--__u8.txt"
0x2 pfc_en @"typedef--__u8.txt"
0x3 tcs_supported @"typedef--__u8.txt"
}
