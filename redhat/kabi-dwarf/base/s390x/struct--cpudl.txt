Version: 1.0
File: kernel/sched/cpudeadline.h:15
Symbol:
Byte size 48
struct cpudl {
0x0 lock @"typedef--raw_spinlock_t.txt"
0x4 size "int"
0x8 free_cpus @"typedef--cpumask_var_t.txt"
0x28 elements * @"struct--cpudl_item.txt"
}
