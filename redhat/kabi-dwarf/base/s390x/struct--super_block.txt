Version: 1.0
File: include/linux/fs.h:1379
Symbol:
Byte size 1280
struct super_block {
0x0 s_list @"struct--list_head.txt"
0x10 s_dev @"typedef--dev_t.txt"
0x14 s_blocksize_bits "unsigned char"
0x18 s_blocksize "long unsigned int"
0x20 s_maxbytes @"typedef--loff_t.txt"
0x28 s_type * @"struct--file_system_type.txt"
0x30 s_op * const @"struct--super_operations.txt"
0x38 dq_op * const @"struct--dquot_operations.txt"
0x40 s_qcop * const @"struct--quotactl_ops.txt"
0x48 s_export_op * const @"struct--export_operations.txt"
0x50 s_flags "long unsigned int"
0x58 s_magic "long unsigned int"
0x60 s_root * @"struct--dentry.txt"
0x68 s_umount @"struct--rw_semaphore.txt"
0x88 s_count "int"
0x8c s_active @"typedef--atomic_t.txt"
0x90 s_security * "void"
0x98 s_xattr * * const @"struct--xattr_handler.txt"
0xa0 s_inodes @"struct--list_head.txt"
0xb0 s_anon @"struct--hlist_bl_head.txt"
0xb8 s_files_deprecated * @"struct--list_head.txt"
0xc0 s_mounts @"struct--list_head.txt"
0xd0 s_dentry_lru @"struct--list_head.txt"
0xe0 s_nr_dentry_unused "long int"
0x100 s_inode_lru_lock @"typedef--spinlock_t.txt"
0x108 s_inode_lru @"struct--list_head.txt"
0x118 s_nr_inodes_unused "long int"
0x120 s_bdev * @"struct--block_device.txt"
0x128 s_bdi * @"struct--backing_dev_info.txt"
0x130 s_mtd * @"<declarations>/struct--mtd_info.txt"
0x138 s_instances @"struct--hlist_node.txt"
0x148 s_dquot @"struct--quota_info.txt"
0x270 s_writers @"struct--sb_writers.txt"
0x320 s_id [32]"char"
0x340 s_uuid [16]@"typedef--u8.txt"
0x350 s_fs_info * "void"
0x358 s_max_links "unsigned int"
0x35c s_mode @"typedef--fmode_t.txt"
0x360 s_time_gran @"typedef--u32.txt"
0x368 s_vfs_rename_mutex @"struct--mutex.txt"
0x390 s_subtype * "char"
0x398 s_options * "char"
0x3a0 s_d_op * const @"struct--dentry_operations.txt"
0x3a8 cleancache_poolid "int"
0x3b0 s_shrink @"struct--shrinker.txt"
0x3e0 s_remove_count @"typedef--atomic_long_t.txt"
0x3e8 s_readonly_remount "int"
0x3f0 s_dio_done_wq * @"struct--workqueue_struct.txt"
0x3f8 rcu @"struct--callback_head.txt"
0x408 s_pins @"struct--hlist_head.txt"
0x410 s_inode_list_lock @"typedef--spinlock_t.txt"
0x418 s_sync_lock @"struct--mutex.txt"
0x440 s_inode_wblist_lock @"typedef--spinlock_t.txt"
0x448 s_inodes_wb @"struct--list_head.txt"
0x458 s_iflags "long unsigned int"
0x460 s_user_ns * @"struct--user_namespace.txt"
0x468 s_fsnotify_inode_refs @"typedef--atomic_long_t.txt"
}
