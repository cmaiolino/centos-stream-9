Version: 1.0
File: include/linux/rmap.h:27
Symbol:
Byte size 72
struct anon_vma {
0x0 root * @"struct--anon_vma.txt"
0x8 rwsem @"struct--rw_semaphore.txt"
0x28 refcount @"typedef--atomic_t.txt"
0x30 rb_root @"struct--rb_root.txt"
0x38 degree "unsigned int"
0x40 parent * @"struct--anon_vma.txt"
}
