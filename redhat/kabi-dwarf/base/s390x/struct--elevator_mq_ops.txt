Version: 1.0
File: include/linux/elevator.h:86
Symbol:
Byte size 184
struct elevator_mq_ops {
0x0 init_sched * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--elevator_type.txt"
)
"int"
0x8 exit_sched * func (NULL) (
(NULL) * @"struct--elevator_queue.txt"
)
"void"
0x10 init_hctx * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
(NULL) "unsigned int"
)
"int"
0x18 exit_hctx * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
(NULL) "unsigned int"
)
"void"
0x20 allow_merge * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
(NULL) * @"struct--bio.txt"
)
@"typedef--bool.txt"
0x28 bio_merge * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
(NULL) * @"struct--bio.txt"
)
@"typedef--bool.txt"
0x30 request_merge * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * * @"struct--request.txt"
(NULL) * @"struct--bio.txt"
)
"int"
0x38 request_merged * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
(NULL) "int"
)
"void"
0x40 requests_merged * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
(NULL) * @"struct--request.txt"
)
"void"
0x48 get_request * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) "unsigned int"
(NULL) * @"struct--blk_mq_alloc_data.txt"
)
* @"struct--request.txt"
0x50 put_request * func (NULL) (
(NULL) * @"struct--request.txt"
)
"void"
0x58 insert_requests * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
(NULL) * @"struct--list_head.txt"
(NULL) @"typedef--bool.txt"
)
"void"
0x60 dispatch_request * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
)
* @"struct--request.txt"
0x68 has_work * func (NULL) (
(NULL) * @"struct--blk_mq_hw_ctx.txt"
)
@"typedef--bool.txt"
0x70 completed_request * func (NULL) (
(NULL) * @"struct--request.txt"
)
"void"
0x78 started_request * func (NULL) (
(NULL) * @"struct--request.txt"
)
"void"
0x80 requeue_request * func (NULL) (
(NULL) * @"struct--request.txt"
)
"void"
0x88 former_request * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
)
* @"struct--request.txt"
0x90 next_request * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
)
* @"struct--request.txt"
0x98 get_rq_priv * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
(NULL) * @"struct--bio.txt"
)
"int"
0xa0 put_rq_priv * func (NULL) (
(NULL) * @"struct--request_queue.txt"
(NULL) * @"struct--request.txt"
)
"void"
0xa8 init_icq * func (NULL) (
(NULL) * @"struct--io_cq.txt"
)
"void"
0xb0 exit_icq * func (NULL) (
(NULL) * @"struct--io_cq.txt"
)
"void"
}
