Version: 1.0
File: net/8021q/vlan.h:28
Symbol:
Byte size 200
struct vlan_info {
0x0 real_dev * @"struct--net_device.txt"
0x8 grp @"struct--vlan_group.txt"
0xa0 vid_list @"struct--list_head.txt"
0xb0 nr_vids "unsigned int"
0xb8 rcu @"struct--callback_head.txt"
}
