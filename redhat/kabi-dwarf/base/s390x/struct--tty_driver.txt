Version: 1.0
File: include/linux/tty_driver.h:290
Symbol:
Byte size 184
struct tty_driver {
0x0 magic "int"
0x4 kref @"struct--kref.txt"
0x8 (NULL) union (NULL) {
cdevs * * @"struct--cdev.txt"
__UNIQUE_ID_rh_kabi_hide33 struct (NULL) {
0x0 cdevs * @"struct--cdev.txt"
}
(NULL) union (NULL) {
}
}
0x10 owner * @"struct--module.txt"
0x18 driver_name * const "char"
0x20 name * const "char"
0x28 name_base "int"
0x2c major "int"
0x30 minor_start "int"
0x34 num "unsigned int"
0x38 type "short int"
0x3a subtype "short int"
0x3c init_termios @"struct--ktermios.txt"
0x68 flags "long unsigned int"
0x70 proc_entry * @"<declarations>/struct--proc_dir_entry.txt"
0x78 other * @"struct--tty_driver.txt"
0x80 ttys * * @"struct--tty_struct.txt"
0x88 ports * * @"struct--tty_port.txt"
0x90 termios * * @"struct--ktermios.txt"
0x98 driver_state * "void"
0xa0 ops * const @"struct--tty_operations.txt"
0xa8 tty_drivers @"struct--list_head.txt"
}
