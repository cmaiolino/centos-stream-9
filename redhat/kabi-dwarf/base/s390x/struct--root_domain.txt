Version: 1.0
File: kernel/sched/sched.h:578
Symbol:
Byte size 5344
struct root_domain {
0x0 refcount @"typedef--atomic_t.txt"
0x4 rto_count @"typedef--atomic_t.txt"
0x8 rcu @"struct--callback_head.txt"
0x18 span @"typedef--cpumask_var_t.txt"
0x38 online @"typedef--cpumask_var_t.txt"
0x58 rto_mask @"typedef--cpumask_var_t.txt"
0x78 cpupri @"struct--cpupri.txt"
0x1468 overload @"typedef--bool.txt"
0x1470 dlo_mask @"typedef--cpumask_var_t.txt"
0x1490 dlo_count @"typedef--atomic_t.txt"
0x1498 dl_bw @"struct--dl_bw.txt"
0x14b0 cpudl @"struct--cpudl.txt"
}
