Version: 1.0
File: include/uapi/linux/time.h:15
Symbol:
Byte size 16
struct timeval {
0x0 tv_sec @"typedef--__kernel_time_t.txt"
0x8 tv_usec @"typedef--__kernel_suseconds_t.txt"
}
