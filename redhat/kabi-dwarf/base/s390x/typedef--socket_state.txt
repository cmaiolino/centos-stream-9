Version: 1.0
File: include/uapi/linux/net.h:53
Symbol:
Byte size 4
typedef socket_state
enum (NULL) {
SS_FREE = 0x0
SS_UNCONNECTED = 0x1
SS_CONNECTING = 0x2
SS_CONNECTED = 0x3
SS_DISCONNECTING = 0x4
}
