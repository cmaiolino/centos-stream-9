Version: 1.0
File: include/net/ip_fib.h:229
Symbol:
Byte size 56
struct fib_table {
0x0 tb_hlist @"struct--hlist_node.txt"
0x10 tb_id @"typedef--u32.txt"
0x14 rh_reserved_tb_default "int"
0x18 tb_num_default "int"
0x20 rcu @"struct--callback_head.txt"
0x30 tb_data * "long unsigned int"
0x38 __data [0]"long unsigned int"
}
