Version: 1.0
File: include/linux/pci.h:706
Symbol:
Byte size 40
struct pci_error_handlers {
0x0 error_detected * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
(NULL) @"enum--pci_channel_state.txt"
)
@"typedef--pci_ers_result_t.txt"
0x8 mmio_enabled * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
@"typedef--pci_ers_result_t.txt"
0x10 link_reset * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
@"typedef--pci_ers_result_t.txt"
0x18 slot_reset * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
@"typedef--pci_ers_result_t.txt"
0x20 resume * func (NULL) (
(NULL) * @"struct--pci_dev.txt"
)
"void"
}
