Version: 1.0
File: include/net/sch_generic.h:263
Symbol:
Byte size 72
struct tcf_proto {
0x0 next * @"struct--tcf_proto.txt"
0x8 root * "void"
0x10 classify * func (NULL) (
(NULL) * @"struct--sk_buff.txt"
(NULL) * const @"struct--tcf_proto.txt"
(NULL) * @"struct--tcf_result.txt"
)
"int"
0x18 protocol @"typedef--__be16.txt"
0x1c prio @"typedef--u32.txt"
0x20 data * "void"
0x28 ops * const @"struct--tcf_proto_ops.txt"
0x30 chain * @"struct--tcf_chain.txt"
0x38 rcu @"struct--callback_head.txt"
}
