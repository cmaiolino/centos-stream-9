Version: 1.0
File: include/uapi/linux/time.h:9
Symbol:
Byte size 16
struct timespec {
0x0 tv_sec @"typedef--__kernel_time_t.txt"
0x8 tv_nsec "long int"
}
