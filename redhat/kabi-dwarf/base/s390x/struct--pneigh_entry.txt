Version: 1.0
File: include/net/neighbour.h:161
Symbol:
Byte size 32
struct pneigh_entry {
0x0 next * @"struct--pneigh_entry.txt"
0x8 net * @"struct--net.txt"
0x10 dev * @"struct--net_device.txt"
0x18 flags @"typedef--u8.txt"
0x19 key [0]@"typedef--u8.txt"
}
