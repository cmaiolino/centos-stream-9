Version: 1.0
File: include/uapi/linux/dcbnl.h:156
Symbol:
Byte size 136
struct ieee_pfc {
0x0 pfc_cap @"typedef--__u8.txt"
0x1 pfc_en @"typedef--__u8.txt"
0x2 mbc @"typedef--__u8.txt"
0x4 delay @"typedef--__u16.txt"
0x8 requests [8]@"typedef--__u64.txt"
0x48 indications [8]@"typedef--__u64.txt"
}
