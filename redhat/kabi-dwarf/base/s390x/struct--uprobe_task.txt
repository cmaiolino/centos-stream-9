Version: 1.0
File: include/linux/uprobes.h:73
Symbol:
Byte size 48
struct uprobe_task {
0x0 state @"enum--uprobe_task_state.txt"
0x4 autask @"struct--arch_uprobe_task.txt"
0x8 return_instances * @"struct--return_instance.txt"
0x10 depth "unsigned int"
0x18 active_uprobe * @"<declarations>/struct--uprobe.txt"
0x20 xol_vaddr "long unsigned int"
0x28 vaddr "long unsigned int"
}
