Version: 1.0
File: include/linux/seq_file.h:18
Symbol:
Byte size 136
struct seq_file {
0x0 buf * "char"
0x8 size @"typedef--size_t.txt"
0x10 from @"typedef--size_t.txt"
0x18 count @"typedef--size_t.txt"
0x20 index @"typedef--loff_t.txt"
0x28 read_pos @"typedef--loff_t.txt"
0x30 version @"typedef--u64.txt"
0x38 lock @"struct--mutex.txt"
0x60 op * const @"struct--seq_operations.txt"
0x68 poll_event "int"
0x70 user_ns * @"struct--user_namespace.txt"
0x78 private * "void"
0x80 file * @"struct--file.txt"
}
