Version: 1.0
File: include/uapi/linux/perf_event.h:306
Symbol:
Byte size 112
struct perf_event_attr {
0x0 type @"typedef--__u32.txt"
0x4 size @"typedef--__u32.txt"
0x8 config @"typedef--__u64.txt"
0x10 (NULL) union (NULL) {
sample_period @"typedef--__u64.txt"
sample_freq @"typedef--__u64.txt"
}
0x18 sample_type @"typedef--__u64.txt"
0x20 read_format @"typedef--__u64.txt"
0x28:0-1 disabled @"typedef--__u64.txt"
0x28:1-2 inherit @"typedef--__u64.txt"
0x28:2-3 pinned @"typedef--__u64.txt"
0x28:3-4 exclusive @"typedef--__u64.txt"
0x28:4-5 exclude_user @"typedef--__u64.txt"
0x28:5-6 exclude_kernel @"typedef--__u64.txt"
0x28:6-7 exclude_hv @"typedef--__u64.txt"
0x28:7-8 exclude_idle @"typedef--__u64.txt"
0x29:0-1 mmap @"typedef--__u64.txt"
0x29:1-2 comm @"typedef--__u64.txt"
0x29:2-3 freq @"typedef--__u64.txt"
0x29:3-4 inherit_stat @"typedef--__u64.txt"
0x29:4-5 enable_on_exec @"typedef--__u64.txt"
0x29:5-6 task @"typedef--__u64.txt"
0x29:6-7 watermark @"typedef--__u64.txt"
0x29:7-9 precise_ip @"typedef--__u64.txt"
0x2a:1-2 mmap_data @"typedef--__u64.txt"
0x2a:2-3 sample_id_all @"typedef--__u64.txt"
0x2a:3-4 exclude_host @"typedef--__u64.txt"
0x2a:4-5 exclude_guest @"typedef--__u64.txt"
0x2a:5-6 exclude_callchain_kernel @"typedef--__u64.txt"
0x2a:6-7 exclude_callchain_user @"typedef--__u64.txt"
0x2a:7-8 mmap2 @"typedef--__u64.txt"
0x2b:0-1 comm_exec @"typedef--__u64.txt"
0x2b:1-2 use_clockid @"typedef--__u64.txt"
0x2b:2-3 context_switch @"typedef--__u64.txt"
0x2b:3-4 write_backward @"typedef--__u64.txt"
0x2b:4-40 __reserved_1 @"typedef--__u64.txt"
0x30 (NULL) union (NULL) {
wakeup_events @"typedef--__u32.txt"
wakeup_watermark @"typedef--__u32.txt"
}
0x34 bp_type @"typedef--__u32.txt"
0x38 (NULL) union (NULL) {
bp_addr @"typedef--__u64.txt"
kprobe_func @"typedef--__u64.txt"
uprobe_path @"typedef--__u64.txt"
config1 @"typedef--__u64.txt"
}
0x40 (NULL) union (NULL) {
bp_len @"typedef--__u64.txt"
kprobe_addr @"typedef--__u64.txt"
probe_offset @"typedef--__u64.txt"
config2 @"typedef--__u64.txt"
}
0x48 branch_sample_type @"typedef--__u64.txt"
0x50 sample_regs_user @"typedef--__u64.txt"
0x58 sample_stack_user @"typedef--__u32.txt"
0x5c clockid @"typedef--__s32.txt"
0x60 sample_regs_intr @"typedef--__u64.txt"
0x68 aux_watermark @"typedef--__u32.txt"
0x6c __reserved_3 @"typedef--__u32.txt"
}
