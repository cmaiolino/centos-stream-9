Version: 1.0
File: include/linux/sysctl.h:135
Symbol:
Byte size 80
struct ctl_table_header {
0x0 (NULL) union (NULL) {
(NULL) struct (NULL) {
0x0 ctl_table * @"struct--ctl_table.txt"
0x8 used "int"
0xc count "int"
0x10 nreg "int"
}
rcu @"struct--callback_head.txt"
}
0x18 unregistering * @"struct--completion.txt"
0x20 ctl_table_arg * @"struct--ctl_table.txt"
0x28 root * @"struct--ctl_table_root.txt"
0x30 set * @"struct--ctl_table_set.txt"
0x38 parent * @"struct--ctl_dir.txt"
0x40 node * @"struct--ctl_node.txt"
0x48 inodes @"struct--hlist_head.txt"
}
